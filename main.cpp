#include "widget.h"
#include "dialog.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    int exitCode=-1;
    do
    {
        Widget w;
        w.show();

        Dialog* dlg=new Dialog(&w);
        dlg->setAttribute(Qt::WA_DeleteOnClose);
        dlg->setWindowModality(Qt::ApplicationModal);
        dlg->show();

        exitCode=a.exec();

    }while(exitCode==888);

    return 0;
}
