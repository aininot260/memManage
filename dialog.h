#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

private slots:

    void on_pushButton_clicked();

    void on_checkBox12_clicked(bool checked);

    void on_checkBox22_clicked(bool checked);

    void on_checkBox32_clicked(bool checked);

    void on_checkBox42_clicked(bool checked);

private:
    Ui::Dialog *ui;
};

#endif // DIALOG_H
