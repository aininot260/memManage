#ifndef EXTERN_H
#define EXTERN_H
#include<vector>
#include<string>
using namespace std;
extern int block_cnt;  //驻留内存页面的个数
extern int mem_time;  //内存的存取时间
extern int int_time;  //缺页中断的时间
extern int tlb_time;  //快表的时间
extern int tlb_cnt;  //快表的容量

extern int algorithm_1;
extern bool is_tlb_1;
extern vector<int> page_1;
extern vector<string> mem_1;

extern int algorithm_2;
extern bool is_tlb_2;
extern vector<int> page_2;
extern vector<string> mem_2;

extern int algorithm_3;
extern bool is_tlb_3;
extern vector<int> page_3;
extern vector<string> mem_3;

extern int algorithm_4;
extern bool is_tlb_4;
extern vector<int> page_4;
extern vector<string> mem_4;

extern bool is_cfg;
#endif // EXTERN_H
