#ifndef TASK_THREAD_H
#define TASK_THREAD_H
#include <QThread>
#include <QMutex>
#include "widget.h"
class TaskThread : public QThread
{
    Q_OBJECT
public:
    TaskThread(Widget *gui);
    ~TaskThread();
protected:
    void run();

signals:
    void signalsUpdateUi(int,int,int,int,QList<int>,int);
    void signalsTid1(int);
    void signalsTid2(int);
    void signalsTid3(int);
    void signalsPid1(int);
    void signalsPid2(int);
    void signalsPid3(int);
    void signalsTid4(int);
    void signalsPos3(int);
public slots:
    void slotUpdateUi(int,int,int,int,QList<int>,int);
    void slotFunc(int);
    void slotSetvt(int);
    void slotSetvp(int);

public:
    Widget *m_gui;  //主线程对象指针
    void pause();
    void resume();
    int *TLB;
    int *PT;
    bool TLB_full;
    bool PT_full;
    int tot_time;
    bool flag;
    int signalFlag;
    int vtisover;
    int vpisover;
private:
    QMutex m_lock;


};
#endif // TASK_THREAD_H
