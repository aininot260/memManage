#ifndef VIS_TLB_H
#define VIS_TLB_H
#include <QThread>
#include "widget.h"
class Vis_TLB : public QThread
{
    Q_OBJECT
public:
    Vis_TLB();
    ~Vis_TLB();
    int tid,pid;
protected:
    void run();
signals:
    signalsSetvt1(int);
    signalsSetvt2(int);
    signalsSetvt3(int);
    signalsSetvt4(int);
public slots:
    void slotTid(int);
    void slotPid(int);
};
#endif // VIS_TLB_H
