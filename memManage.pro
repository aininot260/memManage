#-------------------------------------------------
#
# Project created by QtCreator 2019-01-07T14:11:29
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = memManage
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
        widget.cpp \
    dialog.cpp \
    extern.cpp \
    task_thread.cpp \
    vis_tlb.cpp \
    vis_pt.cpp \
    deal_int.cpp \
    vis_mem.cpp \
    threads.cpp

HEADERS  += widget.h \
    dialog.h \
    extern.h \
    task_thread.h \
    vis_tlb.h \
    vis_pt.h \
    deal_int.h \
    vis_mem.h \
    threads.h

FORMS    += widget.ui \
    dialog.ui
	RC_FILE=logo.rc
