#ifndef VIS_MEM_H
#define VIS_MEM_H
#include <QThread>
#include "widget.h"
class Vis_Mem : public QThread
{
    Q_OBJECT
public:
    Vis_Mem();
    ~Vis_Mem();
    int tid;
protected:
    void run();

public slots:
    void slotTid(int);
};
#endif // VIS_MEM_H
