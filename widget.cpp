#include "widget.h"
#include "dialog.h"
#include "ui_widget.h"
#include "threads.h"
#include "extern.h"
#include<QDebug>
#include<QScrollBar>
#include<QMessageBox>
#include<fstream>
#include<QDateTime>
#include<iomanip>
#include<QDir>
#include<QUrl>
#include<QDesktopServices>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    p1=new TaskThread(this);
    p2=new TaskThread(this);
    p3=new TaskThread(this);
    p4=new TaskThread(this);

    connect(this, SIGNAL(signalsFunc1(int)), p1, SLOT(slotFunc(int)));
    connect(this, SIGNAL(signalsFunc2(int)), p2, SLOT(slotFunc(int)));
    connect(this, SIGNAL(signalsFunc3(int)), p3, SLOT(slotFunc(int)));
    connect(this, SIGNAL(signalsFunc4(int)), p4, SLOT(slotFunc(int)));

    model11= new QStandardItemModel();
    ui->tableViewB1->setModel(model11);
    model11->setRowCount(3);
    model11->setHeaderData(0,Qt::Vertical, "当前页面");
    model11->setHeaderData(1,Qt::Vertical, "存取时间");
    model11->setHeaderData(2,Qt::Vertical, "是否缺页");

    model12= new QStandardItemModel();
    ui->tableViewB2->setModel(model12);
    model12->setRowCount(3);
    model12->setHeaderData(0,Qt::Vertical, "当前页面");
    model12->setHeaderData(1,Qt::Vertical, "存取时间");
    model12->setHeaderData(2,Qt::Vertical, "是否缺页");

    model13= new QStandardItemModel();
    ui->tableViewB3->setModel(model13);
    model13->setRowCount(3);
    model13->setHeaderData(0,Qt::Vertical, "当前页面");
    model13->setHeaderData(1,Qt::Vertical, "存取时间");
    model13->setHeaderData(2,Qt::Vertical, "是否缺页");

    model14= new QStandardItemModel();
    ui->tableViewB4->setModel(model14);
    model14->setRowCount(3);
    model14->setHeaderData(0,Qt::Vertical, "当前页面");
    model14->setHeaderData(1,Qt::Vertical, "存取时间");
    model14->setHeaderData(2,Qt::Vertical, "是否缺页");

    model21 = new QStandardItemModel();
    ui->tableViewS1->setModel(model21);
    model21->setRowCount(1);
    model21->setHeaderData(0,Qt::Vertical, "物理块");

    model22 = new QStandardItemModel();
    ui->tableViewS2->setModel(model22);
    model22->setRowCount(1);
    model22->setHeaderData(0,Qt::Vertical, "物理块");

    model23 = new QStandardItemModel();
    ui->tableViewS3->setModel(model23);
    model23->setRowCount(1);
    model23->setHeaderData(0,Qt::Vertical, "物理块");

    model24 = new QStandardItemModel();
    ui->tableViewS4->setModel(model24);
    model24->setRowCount(1);
    model24->setHeaderData(0,Qt::Vertical, "物理块");

    model_cnt1=prev_time1=null_cnt1=0;
    model_cnt2=prev_time2=null_cnt2=0;
    model_cnt3=prev_time3=null_cnt3=0;
    model_cnt4=prev_time4=null_cnt4=0;
}

Widget::~Widget()
{
    delete ui;
    delete p1;
    delete p2;
    delete p3;
    delete p4;
    delete model11;
    delete model12;
    delete model13;
    delete model14;
    delete model21;
    delete model22;
    delete model23;
    delete model24;
}

void Widget::on_pushButtonCfg_clicked()
{
    Dialog* dlg=new Dialog(this);
    dlg->setAttribute(Qt::WA_DeleteOnClose);
    dlg->setWindowModality(Qt::ApplicationModal);
    dlg->show();
}

void Widget::on_pushButtonRun_clicked()
{
    if(!is_cfg)
    {
        QMessageBox::warning(NULL, "警告", "请先进行参数配置。", QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
        return;
    }
    ui->pushButtonRun->setEnabled(false);
    ui->pushButtonStop->setEnabled(true);

    ui->pushButtonR1->setEnabled(false);
    ui->pushButtonR2->setEnabled(false);
    ui->pushButtonR3->setEnabled(false);
    ui->pushButtonR4->setEnabled(false);
    ui->pushButtonE1->setEnabled(true);
    ui->pushButtonE2->setEnabled(true);
    ui->pushButtonE3->setEnabled(true);
    ui->pushButtonE4->setEnabled(true);
    ui->pushButtonCfg->setEnabled(false);

    p1->start();
    p2->start();
    p3->start();
    p4->start();

    emit signalsFunc1(1);
    emit signalsFunc2(2);
    emit signalsFunc3(3);
    emit signalsFunc4(4);
}

void Widget::on_pushButtonStop_clicked()
{
    if(ui->pushButtonE1->isEnabled()==false
    &&ui->pushButtonE2->isEnabled()==false
    &&ui->pushButtonE3->isEnabled()==false
    &&ui->pushButtonE4->isEnabled()==false)
    {
        p1->terminate();
        p1->wait();
        p2->terminate();
        p2->wait();
        p3->terminate();
        p3->wait();
        p4->terminate();
        p4->wait();
        qApp->exit(888);
    }
    else
    {
        QMessageBox::warning(NULL, "警告", "请暂停所有任务之后再按停止按钮。", QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
        return;
    }
}

void Widget::on_pushButtonR1_clicked()
{
    ui->pushButtonR1->setEnabled(false);
    ui->pushButtonE1->setEnabled(true);
    p1->resume();
}

void Widget::on_pushButtonE1_clicked()
{
    ui->pushButtonE1->setEnabled(false);
    ui->pushButtonR1->setEnabled(true);
    p1->pause();
}

void Widget::on_pushButtonR2_clicked()
{
    ui->pushButtonR2->setEnabled(false);
    ui->pushButtonE2->setEnabled(true);
    p2->resume();
}

void Widget::on_pushButtonE2_clicked()
{
    ui->pushButtonE2->setEnabled(false);
    ui->pushButtonR2->setEnabled(true);
    p2->pause();
}

void Widget::on_pushButtonR3_clicked()
{
    ui->pushButtonR3->setEnabled(false);
    ui->pushButtonE3->setEnabled(true);
    p3->resume();
}

void Widget::on_pushButtonE3_clicked()
{
    ui->pushButtonE3->setEnabled(false);
    ui->pushButtonR3->setEnabled(true);
    p3->pause();
}

void Widget::on_pushButtonR4_clicked()
{
    ui->pushButtonR4->setEnabled(false);
    ui->pushButtonE4->setEnabled(true);
    p4->resume();
}

void Widget::on_pushButtonE4_clicked()
{
    ui->pushButtonE4->setEnabled(false);
    ui->pushButtonR4->setEnabled(true);
    p4->pause();
}
void Widget::Display(int page_tot,int time_tot,int which_page,int is_null,QList<int> q,int whicht)
{
    if(whicht==1)
    {
        model11->setItem(0, model_cnt1, new QStandardItem(QString::number(which_page)));
        model11->setItem(1, model_cnt1, new QStandardItem(QString::number(time_tot-prev_time1)+"ns"));
        if(is_null==1)
        {
            model11->setItem(2, model_cnt1, new QStandardItem("√"));
            null_cnt1++;
        }

        ui->progressBarT1->setValue(1.0*(time_tot-prev_time1)/(3*mem_time+int_time)*100);
        prev_time1=time_tot;
        ui->tableViewB1->horizontalScrollBar()->setSliderPosition(ui->tableViewB1->horizontalScrollBar()->maximum());
        model_cnt1++;
        ui->tableViewB1->repaint();


        for(int i=0;i<q.size();i++)
        {
            model21->setItem(0, i, new QStandardItem(QString::number(q.at(i))));
        }
        ui->tableViewS1->horizontalScrollBar()->setSliderPosition(ui->tableViewS1->horizontalScrollBar()->maximum());
        ui->tableViewS1->repaint();

        ui->labelT1->setText("总耗时："+QString::number(time_tot)+"ns");
        ui->labelP1->setText("缺页次数："+QString::number(null_cnt1));
        ui->labelD1->setText("缺页率："+ QString::number((floor((1.0*null_cnt1/page_tot)*10000.000f+0.5)/10000.000f)*100)+"%");

        ui->progressBarP1->setValue(1.0*model_cnt1/page_tot*100);
    }
    if(whicht==2)
    {
        model12->setItem(0, model_cnt2, new QStandardItem(QString::number(which_page)));
        model12->setItem(1, model_cnt2, new QStandardItem(QString::number(time_tot-prev_time2)+"ns"));
        if(is_null==1)
        {
            model12->setItem(2, model_cnt2, new QStandardItem("√"));
            null_cnt2++;
        }

        ui->progressBarT2->setValue(1.0*(time_tot-prev_time2)/(3*mem_time+int_time)*100);
        prev_time2=time_tot;
        ui->tableViewB2->horizontalScrollBar()->setSliderPosition(ui->tableViewB2->horizontalScrollBar()->maximum());
        model_cnt2++;
        ui->tableViewB2->repaint();


        for(int i=0;i<q.size();i++)
        {
            model22->setItem(0, i, new QStandardItem(QString::number(q.at(i))));
        }
        ui->tableViewS2->horizontalScrollBar()->setSliderPosition(ui->tableViewS2->horizontalScrollBar()->maximum());
        ui->tableViewS2->repaint();

        ui->labelT2->setText("总耗时："+QString::number(time_tot)+"ns");
        ui->labelP2->setText("缺页次数："+QString::number(null_cnt2));
        ui->labelD2->setText("缺页率："+ QString::number((floor((1.0*null_cnt2/page_tot)*10000.000f+0.5)/10000.000f)*100)+"%");

        ui->progressBarP2->setValue(1.0*model_cnt2/page_tot*100);
    }
    if(whicht==3)
    {
        model13->setItem(0, model_cnt3, new QStandardItem(QString::number(which_page)));
        model13->setItem(1, model_cnt3, new QStandardItem(QString::number(time_tot-prev_time3)+"ns"));
        if(is_null==1)
        {
            model13->setItem(2, model_cnt3, new QStandardItem("√"));
            null_cnt3++;
        }

        ui->progressBarT3->setValue(1.0*(time_tot-prev_time3)/(3*mem_time+int_time)*100);
        prev_time3=time_tot;
        ui->tableViewB3->horizontalScrollBar()->setSliderPosition(ui->tableViewB3->horizontalScrollBar()->maximum());
        model_cnt3++;
        ui->tableViewB3->repaint();


        for(int i=0;i<q.size();i++)
        {
            model23->setItem(0, i, new QStandardItem(QString::number(q.at(i))));
        }
        ui->tableViewS3->horizontalScrollBar()->setSliderPosition(ui->tableViewS3->horizontalScrollBar()->maximum());
        ui->tableViewS3->repaint();

        ui->labelT3->setText("总耗时："+QString::number(time_tot)+"ns");
        ui->labelP3->setText("缺页次数："+QString::number(null_cnt3));
        ui->labelD3->setText("缺页率："+ QString::number((floor((1.0*null_cnt3/page_tot)*10000.000f+0.5)/10000.000f)*100)+"%");

        ui->progressBarP3->setValue(1.0*model_cnt3/page_tot*100);
    }
    if(whicht==4)
    {
        model14->setItem(0, model_cnt4, new QStandardItem(QString::number(which_page)));
        model14->setItem(1, model_cnt4, new QStandardItem(QString::number(time_tot-prev_time4)+"ns"));
        if(is_null==1)
        {
            model14->setItem(2, model_cnt4, new QStandardItem("√"));
            null_cnt4++;
        }

        ui->progressBarT4->setValue(1.0*(time_tot-prev_time4)/(3*mem_time+int_time)*100);
        prev_time4=time_tot;
        ui->tableViewB4->horizontalScrollBar()->setSliderPosition(ui->tableViewB4->horizontalScrollBar()->maximum());
        model_cnt4++;
        ui->tableViewB4->repaint();


        for(int i=0;i<q.size();i++)
        {
            model24->setItem(0, i, new QStandardItem(QString::number(q.at(i))));
        }
        ui->tableViewS4->horizontalScrollBar()->setSliderPosition(ui->tableViewS4->horizontalScrollBar()->maximum());
        ui->tableViewS4->repaint();

        ui->labelT4->setText("总耗时："+QString::number(time_tot)+"ns");
        ui->labelP4->setText("缺页次数："+QString::number(null_cnt4));
        ui->labelD4->setText("缺页率："+ QString::number((floor((1.0*null_cnt4/page_tot)*10000.000f+0.5)/10000.000f)*100)+"%");

        ui->progressBarP4->setValue(1.0*model_cnt4/page_tot*100);
    }
}
string get_algor(int x)
{
    string ret="";
    switch(x)
    {
        case 1:
            ret="FIFO算法";
        break;
    case 2:
        ret="LRU算法";
                break;
    case 3:
        ret="OPT算法";
        break;
    default:
        ret="SLNN算法";
    }
    return ret;
}
string get_istlb(int x)
{
    if(x==1)
        return "是";
    else return "否";
}

void Widget::on_pushButtonSave_clicked()
{
    QDateTime current_date_time =QDateTime::currentDateTime();
    QString current_date =current_date_time.toString("yyyy_MM_dd_hh_mm_ss");
    current_date=current_date+".txt";
    qDebug()<<current_date;
    ofstream out(current_date.toStdString());
    //**********************************************************
    out<<"多线程页面置换算法模拟数据导出记录:"<<endl;
    out<<"驻留内存页面的个数:"<<block_cnt<<endl;
    out<<"内存的存取时间:"<<mem_time<<endl;
    out<<"缺页中断的时间:"<<int_time<<endl;
    out<<"快表的访问时间:"<<tlb_time<<endl;
    out<<"快表的容量:"<<tlb_cnt<<endl;
    out<<endl;

    out<<"算法1："<<get_algor(algorithm_1)<<endl;
    out<<"是否使用快表:"<<get_istlb(is_tlb_1)<<endl;
    out<<"访问的页面序列："<<endl;
    for(unsigned int i=0;i<page_1.size();i++)
        out<<page_1[i]<<",";
    out<<endl;
    out<<"访问的逻辑地址："<<endl;
    for(unsigned int i=0;i<mem_1.size();i++)
        out<<mem_1[i]<<",";
    out<<endl;
    out<<"访问时间："<<endl;
    out<<setiosflags(ios::right)<<setiosflags(ios::fixed);
    for(int i=1;i<model11->rowCount()-1;i++)
    {
        for(int j=0;j<model11->columnCount();j++)
        {
            QModelIndex index=model11->index(i,j,QModelIndex());
            QString str=index.data().toString();
            out<<str.toStdString()<<",";
        }
        out<<endl;
    }
    out<<endl;

    out<<"算法2："<<get_algor(algorithm_2)<<endl;
    out<<"是否使用快表:"<<get_istlb(is_tlb_2)<<endl;
    out<<"访问的页面序列："<<endl;
    for(unsigned int i=0;i<page_2.size();i++)
        out<<page_2[i]<<",";
    out<<endl;
    out<<"访问的逻辑地址："<<endl;
    for(unsigned int i=0;i<mem_2.size();i++)
        out<<mem_2[i]<<",";
    out<<endl;
    out<<"访问时间："<<endl;
    out<<setiosflags(ios::right)<<setiosflags(ios::fixed);
    for(int i=1;i<model12->rowCount()-1;i++)
    {
        for(int j=0;j<model12->columnCount();j++)
        {
            QModelIndex index=model12->index(i,j,QModelIndex());
            QString str=index.data().toString();
            out<<str.toStdString()<<",";
        }
        out<<endl;
    }
    out<<endl;

    out<<"算法3："<<get_algor(algorithm_3)<<endl;
    out<<"是否使用快表:"<<get_istlb(is_tlb_3)<<endl;
    out<<"访问的页面序列："<<endl;
    for(unsigned int i=0;i<page_3.size();i++)
        out<<page_3[i]<<",";
    out<<endl;
    out<<"访问的逻辑地址："<<endl;
    for(unsigned int i=0;i<mem_3.size();i++)
        out<<mem_3[i]<<",";
    out<<endl;
    out<<"访问时间："<<endl;
    out<<setiosflags(ios::right)<<setiosflags(ios::fixed);
    for(int i=1;i<model13->rowCount()-1;i++)
    {
        for(int j=0;j<model13->columnCount();j++)
        {
            QModelIndex index=model13->index(i,j,QModelIndex());
            QString str=index.data().toString();
            out<<str.toStdString()<<",";
        }
        out<<endl;
    }
    out<<endl;

    out<<"算法4："<<get_algor(algorithm_4)<<endl;
    out<<"是否使用快表:"<<get_istlb(is_tlb_4)<<endl;
    out<<"访问的页面序列："<<endl;
    for(unsigned int i=0;i<page_4.size();i++)
        out<<page_4[i]<<",";
    out<<endl;
    out<<"访问的逻辑地址："<<endl;
    for(unsigned int i=0;i<mem_4.size();i++)
        out<<mem_4[i]<<",";
    out<<endl;
    out<<"访问时间："<<endl;
    out<<setiosflags(ios::right)<<setiosflags(ios::fixed);
    for(int i=1;i<model14->rowCount()-1;i++)
    {
        for(int j=0;j<model14->columnCount();j++)
        {
            QModelIndex index=model14->index(i,j,QModelIndex());
            QString str=index.data().toString();
            out<<str.toStdString()<<",";
        }
        out<<endl;
    }
    out<<endl;
    //**********************************************************
    qDebug()<<QDir::currentPath();
    QDesktopServices::openUrl(QUrl(QDir::currentPath(), QUrl::TolerantMode));
}
