#include <QtDebug>
#include "threads.h"
#include "extern.h"
#include <QMetaType>
TaskThread::TaskThread(Widget *gui)
{
    m_gui = gui;
    TLB=new int[tlb_cnt+1];
    PT=new int[block_cnt+1];
    TLB_full=0;
    PT_full=0;
    tot_time=0;
    flag=0;
    signalFlag=0;
    vtisover=-1;
    vpisover=-1;
    qRegisterMetaType<QList<int>>("QList<int>");
    connect(this, SIGNAL(signalsUpdateUi(int,int,int,int,QList<int>,int)), this, SLOT(slotUpdateUi(int,int,int,int,QList<int>,int)));  //可以传参，参数一致即可
}
TaskThread::~TaskThread()
{
    delete TLB;
    delete PT;
}
void TaskThread::run()
{
    //qDebug()<<"start";

    for(int i=0;i<tlb_cnt;i++) TLB[i]=0;
    for(int j=0;j<block_cnt;j++) PT[j]=0;
    while(signalFlag)
    {
        if(signalFlag==1)
        {
            //执行算法1
            vt1=new Vis_TLB();
            vp1=new Vis_PT();
            di1=new Deal_Int();
            vm1=new Vis_Mem();
            connect(this, SIGNAL(signalsTid1(int)), vt1, SLOT(slotTid(int)));
            connect(this, SIGNAL(signalsTid2(int)), vp1, SLOT(slotTid(int)));
            connect(this, SIGNAL(signalsTid3(int)), di1, SLOT(slotTid(int)));
            connect(this, SIGNAL(signalsPid1(int)), vt1, SLOT(slotPid(int)));
            connect(this, SIGNAL(signalsPid2(int)), vp1, SLOT(slotPid(int)));
            connect(this, SIGNAL(signalsPid3(int)), di1, SLOT(slotPid(int)));
            connect(this, SIGNAL(signalsTid4(int)), vm1, SLOT(slotTid(int)));
            connect(this, SIGNAL(signalsPos3(int)), di1, SLOT(slotPos(int)));
            for(unsigned int i=0;i<page_1.size();i++)
            {
                //同时在快表和慢表里找
                if(is_tlb_1)
                    vt1->start();
                vp1->start();
                flag=0;

                if(is_tlb_1)
                {
                    emit signalsTid1(1);
                    emit signalsPid1(page_1[i]);
                }
                emit signalsTid2(1);
                emit signalsPid2(page_1[i]);

                if(is_tlb_1)
                {
                    vt1->wait();
                }
                vp1->wait();

                while(1)
                {
                    if(is_tlb_1)
                    {
                        if(vtisover>=0&&vpisover>=0)
                        {
                            if(vtisover==1)
                                tot_time+=tlb_time;
                            else if(vtisover==0)
                                tot_time+=mem_time;
                            vt1->exit();
                            vp1->exit();
                            break;
                        }
                    }
                    else
                    {
                        if(vpisover>=0)
                        {
                            tot_time+=mem_time;
                            vp1->exit();
                            break;
                        }
                    }
                }
                vtisover=vpisover=-1;
                int flag1=flag;
                if(!flag)  //缺页中断
                {
                    di1->start();
                    emit signalsTid3(1);
                    emit signalsPid3(page_1[i]);
                    emit signalsPos3(i);

                    di1->wait();
                    di1->exit();

                    //qDebug()<<"?"<<TLB[0]<<TLB[1];

                    if(is_tlb_1)
                        vt1->start();
                    vp1->start();
                    flag=0;

                    if(is_tlb_1)
                    {
                        emit signalsTid1(1);
                        emit signalsPid1(page_1[i]);
                    }
                    emit signalsTid2(1);
                    emit signalsPid2(page_1[i]);

                    if(is_tlb_1)
                    {
                        vt1->wait();
                    }
                    vp1->wait();

                    while(1)
                    {
                        if(is_tlb_1)
                        {
                            if(vtisover>=0&&vpisover>=0)
                            {
                                if(vtisover==1)
                                    tot_time+=tlb_time;
                                else if(vtisover==0)
                                    tot_time+=mem_time;
                                vt1->exit();
                                vp1->exit();
                                break;
                            }
                        }
                        else
                        {
                            if(vpisover>=0)
                            {
                                tot_time+=mem_time;
                                vp1->exit();
                                break;
                            }
                        }
                    }
                }
                //else qDebug()<<TLB[0]<<TLB[1];


                vm1->start();
                emit signalsTid4(1);
                vm1->wait();
                vm1->exit();
                QList<int> tmpq;
                for(int i=0;i<block_cnt;i++)
                    tmpq.append(p1->PT[i]);

                m_lock.lock();
                emit signalsUpdateUi(int(page_1.size()),int(tot_time),int(page_1[i]),int(flag1^1),QList<int>(tmpq),1);  //给主线程发信号来更新UI
                m_lock.unlock();
            }
            flag=0;

            delete vt1;
            delete vp1;
            delete di1;
            delete vm1;
        }
        if(signalFlag==2)
        {
            //执行算法2
            vt2=new Vis_TLB();
            vp2=new Vis_PT();
            di2=new Deal_Int();
            vm2=new Vis_Mem();
            connect(this, SIGNAL(signalsTid1(int)), vt2, SLOT(slotTid(int)));
            connect(this, SIGNAL(signalsTid2(int)), vp2, SLOT(slotTid(int)));
            connect(this, SIGNAL(signalsTid3(int)), di2, SLOT(slotTid(int)));
            connect(this, SIGNAL(signalsPid1(int)), vt2, SLOT(slotPid(int)));
            connect(this, SIGNAL(signalsPid2(int)), vp2, SLOT(slotPid(int)));
            connect(this, SIGNAL(signalsPid3(int)), di2, SLOT(slotPid(int)));
            connect(this, SIGNAL(signalsTid4(int)), vm2, SLOT(slotTid(int)));
            connect(this, SIGNAL(signalsPos3(int)), di2, SLOT(slotPos(int)));
            for(unsigned int i=0;i<page_2.size();i++)
            {
                //同时在快表和慢表里找
                if(is_tlb_2)
                    vt2->start();
                vp2->start();
                flag=0;

                if(is_tlb_2)
                {
                    emit signalsTid1(2);
                    emit signalsPid1(page_2[i]);
                }
                emit signalsTid2(2);
                emit signalsPid2(page_2[i]);

                if(is_tlb_2)
                {
                    vt2->wait();
                }
                vp2->wait();

                while(1)
                {
                    if(is_tlb_2)
                    {
                        if(vtisover>=0&&vpisover>=0)
                        {
                            if(vtisover==1)
                                tot_time+=tlb_time;
                            else if(vtisover==0)
                                tot_time+=mem_time;
                            vt2->exit();
                            vp2->exit();
                            break;
                        }
                    }
                    else
                    {
                        if(vpisover>=0)
                        {
                            tot_time+=mem_time;
                            vp2->exit();
                            break;
                        }
                    }
                }
                vtisover=vpisover=-1;
                int flag1=flag;
                if(!flag)  //缺页中断
                {
                    di2->start();
                    emit signalsTid3(2);
                    emit signalsPid3(page_2[i]);
                    emit signalsPos3(i);

                    di2->wait();
                    di2->exit();

                    //qDebug()<<"?"<<TLB[0]<<TLB[1];

                    if(is_tlb_2)
                        vt2->start();
                    vp2->start();
                    flag=0;

                    if(is_tlb_2)
                    {
                        emit signalsTid1(2);
                        emit signalsPid1(page_2[i]);
                    }
                    emit signalsTid2(2);
                    emit signalsPid2(page_2[i]);

                    if(is_tlb_2)
                    {
                        vt2->wait();
                    }
                    vp2->wait();

                    while(1)
                    {
                        if(is_tlb_2)
                        {
                            if(vtisover>=0&&vpisover>=0)
                            {
                                if(vtisover==1)
                                    tot_time+=tlb_time;
                                else if(vtisover==0)
                                    tot_time+=mem_time;
                                vt2->exit();
                                vp2->exit();
                                break;
                            }
                        }
                        else
                        {
                            if(vpisover>=0)
                            {
                                tot_time+=mem_time;
                                vp2->exit();
                                break;
                            }
                        }
                    }
                }
                //else qDebug()<<TLB[0]<<TLB[1];


                vm2->start();
                emit signalsTid4(2);
                vm2->wait();
                vm2->exit();
                QList<int> tmpq;
                for(int i=0;i<block_cnt;i++)
                    tmpq.append(p2->PT[i]);

                m_lock.lock();
                emit signalsUpdateUi(int(page_2.size()),int(tot_time),int(page_2[i]),int(flag1^1),QList<int>(tmpq),2);  //给主线程发信号来更新UI
                m_lock.unlock();
            }
            flag=0;

            delete vt2;
            delete vp2;
            delete di2;
            delete vm2;
        }
        if(signalFlag==3)
        {
            //执行算法3
            vt3=new Vis_TLB();
            vp3=new Vis_PT();
            di3=new Deal_Int();
            vm3=new Vis_Mem();
            connect(this, SIGNAL(signalsTid1(int)), vt3, SLOT(slotTid(int)));
            connect(this, SIGNAL(signalsTid2(int)), vp3, SLOT(slotTid(int)));
            connect(this, SIGNAL(signalsTid3(int)), di3, SLOT(slotTid(int)));
            connect(this, SIGNAL(signalsPid1(int)), vt3, SLOT(slotPid(int)));
            connect(this, SIGNAL(signalsPid2(int)), vp3, SLOT(slotPid(int)));
            connect(this, SIGNAL(signalsPid3(int)), di3, SLOT(slotPid(int)));
            connect(this, SIGNAL(signalsTid4(int)), vm3, SLOT(slotTid(int)));
            connect(this, SIGNAL(signalsPos3(int)), di3, SLOT(slotPos(int)));
            for(unsigned int i=0;i<page_3.size();i++)
            {

                //同时在快表和慢表里找
                if(is_tlb_3)
                    vt3->start();
                vp3->start();
                flag=0;

                if(is_tlb_3)
                {
                    emit signalsTid1(3);
                    emit signalsPid1(page_3[i]);
                }
                emit signalsTid2(3);
                emit signalsPid2(page_3[i]);

                if(is_tlb_3)
                {
                    vt3->wait();
                }
                vp3->wait();

                while(1)
                {
                    if(is_tlb_3)
                    {
                        if(vtisover>=0&&vpisover>=0)
                        {
                            if(vtisover==1)
                                tot_time+=tlb_time;
                            else if(vtisover==0)
                                tot_time+=mem_time;
                            vt3->exit();
                            vp3->exit();
                            break;
                        }
                    }
                    else
                    {
                        if(vpisover>=0)
                        {
                            tot_time+=mem_time;
                            vp3->exit();
                            break;
                        }
                    }
                }
                int flag1=flag;
                vtisover=vpisover=-1;
                if(!flag)  //缺页中断
                {
                    di3->start();
                    emit signalsTid3(3);
                    emit signalsPid3(page_3[i]);
                    emit signalsPos3(i);

                    di3->wait();
                    di3->exit();

                    //qDebug()<<"?"<<TLB[0]<<TLB[1];

                    if(is_tlb_3)
                        vt3->start();
                    vp3->start();
                    flag=0;

                    if(is_tlb_3)
                    {
                        emit signalsTid1(3);
                        emit signalsPid1(page_3[i]);
                    }
                    emit signalsTid2(3);
                    emit signalsPid2(page_3[i]);

                    if(is_tlb_3)
                    {
                        vt3->wait();
                    }
                    vp3->wait();

                    while(1)
                    {
                        if(is_tlb_3)
                        {
                            if(vtisover>=0&&vpisover>=0)
                            {
                                if(vtisover==1)
                                    tot_time+=tlb_time;
                                else if(vtisover==0)
                                    tot_time+=mem_time;
                                vt3->exit();
                                vp3->exit();
                                break;
                            }
                        }
                        else
                        {
                            if(vpisover>=0)
                            {
                                tot_time+=mem_time;
                                vp3->exit();
                                break;
                            }
                        }
                    }
                }
                //else qDebug()<<TLB[0]<<TLB[1];


                vm3->start();
                emit signalsTid4(3);
                vm3->wait();
                vm3->exit();
                QList<int> tmpq;
                for(int i=0;i<block_cnt;i++)
                    tmpq.append(p3->PT[i]);

                m_lock.lock();
                emit signalsUpdateUi(int(page_3.size()),int(tot_time),int(page_3[i]),int(flag1^1),QList<int>(tmpq),3);  //给主线程发信号来更新UI
                m_lock.unlock();
            }
            flag=0;

            delete vt3;
            delete vp3;
            delete di3;
            delete vm3;
        }
        if(signalFlag==4)
        {
            //执行算法4
            vt4=new Vis_TLB();
            vp4=new Vis_PT();
            di4=new Deal_Int();
            vm4=new Vis_Mem();
            connect(this, SIGNAL(signalsTid1(int)), vt4, SLOT(slotTid(int)));
            connect(this, SIGNAL(signalsTid2(int)), vp4, SLOT(slotTid(int)));
            connect(this, SIGNAL(signalsTid3(int)), di4, SLOT(slotTid(int)));
            connect(this, SIGNAL(signalsPid1(int)), vt4, SLOT(slotPid(int)));
            connect(this, SIGNAL(signalsPid2(int)), vp4, SLOT(slotPid(int)));
            connect(this, SIGNAL(signalsPid3(int)), di4, SLOT(slotPid(int)));
            connect(this, SIGNAL(signalsTid4(int)), vm4, SLOT(slotTid(int)));
            connect(this, SIGNAL(signalsPos3(int)), di4, SLOT(slotPos(int)));
            for(unsigned int i=0;i<page_4.size();i++)
            {

                //同时在快表和慢表里找
                if(is_tlb_4)
                    vt4->start();
                vp4->start();
                flag=0;

                if(is_tlb_4)
                {
                    emit signalsTid1(4);
                    emit signalsPid1(page_4[i]);
                }
                emit signalsTid2(4);
                emit signalsPid2(page_4[i]);

                if(is_tlb_4)
                {
                    vt4->wait();
                }
                vp4->wait();

                while(1)
                {
                    if(is_tlb_4)
                    {
                        if(vtisover>=0&&vpisover>=0)
                        {
                            if(vtisover==1)
                                tot_time+=tlb_time;
                            else if(vtisover==0)
                                tot_time+=mem_time;
                            vt4->exit();
                            vp4->exit();
                            break;
                        }
                    }
                    else
                    {
                        if(vpisover>=0)
                        {
                            tot_time+=mem_time;
                            vp4->exit();
                            break;
                        }
                    }
                }
                int flag1=flag;
                vtisover=vpisover=-1;
                if(!flag)  //缺页中断
                {
                    di4->start();
                    emit signalsTid3(4);
                    emit signalsPid3(page_4[i]);
                    emit signalsPos3(i);

                    di4->wait();
                    di4->exit();

                    //qDebug()<<"?"<<TLB[0]<<TLB[1];

                    if(is_tlb_4)
                        vt4->start();
                    vp4->start();
                    flag=0;

                    if(is_tlb_4)
                    {
                        emit signalsTid1(4);
                        emit signalsPid1(page_4[i]);
                    }
                    emit signalsTid2(4);
                    emit signalsPid2(page_4[i]);

                    if(is_tlb_4)
                    {
                        vt4->wait();
                    }
                    vp4->wait();

                    while(1)
                    {
                        if(is_tlb_4)
                        {
                            if(vtisover>=0&&vpisover>=0)
                            {
                                if(vtisover==1)
                                    tot_time+=tlb_time;
                                else if(vtisover==0)
                                    tot_time+=mem_time;
                                vt4->exit();
                                vp4->exit();
                                break;
                            }
                        }
                        else
                        {
                            if(vpisover>=0)
                            {
                                tot_time+=mem_time;
                                vp4->exit();
                                break;
                            }
                        }
                    }
                }
                //else qDebug()<<TLB[0]<<TLB[1];


                vm4->start();
                emit signalsTid4(4);
                vm4->wait();
                vm4->exit();
                QList<int> tmpq;
                for(int i=0;i<block_cnt;i++)
                    tmpq.append(p4->PT[i]);
                m_lock.lock();
                emit signalsUpdateUi(int(page_4.size()),int(tot_time),int(page_4[i]),int(flag1^1),QList<int>(tmpq),4);  //给主线程发信号来更新UI
                m_lock.unlock();
            }
            flag=0;

            delete vt4;
            delete vp4;
            delete di4;
            delete vm4;
        }
        break;
    }
    //qDebug()<<"end";
}
void TaskThread::slotUpdateUi(int page_tot,int time_tot,int which_page,int is_null,QList<int> q,int whicht)
{
    m_gui->Display(page_tot,time_tot,which_page,is_null,q,whicht);

}
void TaskThread::slotFunc(int x)
{
     signalFlag = x;
}
void TaskThread::slotSetvt(int x)
{
      vtisover= x;
}
void TaskThread::slotSetvp(int x)
{
      vpisover= x;
}
void TaskThread::pause()
{
    m_lock.lock();
}
void TaskThread::resume()
{
    m_lock.unlock();
}
