#ifndef DEAL_INT_H
#define DEAL_INT_H
#include <QThread>
#include "widget.h"
class Deal_Int : public QThread
{
    Q_OBJECT
public:
    Deal_Int();
    ~Deal_Int();
    int tid,pid,pos;
protected:
    void run();

public slots:
    void slotPos(int);
    void slotTid(int);
    void slotPid(int);
};
#endif // DEAL_INT_H
