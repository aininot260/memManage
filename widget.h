#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include<QStandardItemModel>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    void Display(int,int,int,int,QList<int>,int);
    QStandardItemModel *model11,*model12,*model13,*model14;
    QStandardItemModel *model21,*model22,*model23,*model24;
    int model_cnt1,prev_time1,null_cnt1;
    int model_cnt2,prev_time2,null_cnt2;
    int model_cnt3,prev_time3,null_cnt3;
    int model_cnt4,prev_time4,null_cnt4;
    ~Widget();
signals:
    signalsFunc1(int);
    signalsFunc2(int);
    signalsFunc3(int);
    signalsFunc4(int);
private slots:
    void on_pushButtonCfg_clicked();

    void on_pushButtonRun_clicked();

    void on_pushButtonStop_clicked();

    void on_pushButtonR1_clicked();

    void on_pushButtonE1_clicked();

    void on_pushButtonR2_clicked();

    void on_pushButtonE2_clicked();

    void on_pushButtonR3_clicked();

    void on_pushButtonE3_clicked();

    void on_pushButtonR4_clicked();

    void on_pushButtonE4_clicked();

    void on_pushButtonSave_clicked();

private:
    Ui::Widget *ui;
};

#endif // WIDGET_H
