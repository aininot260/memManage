#include <QtDebug>
#include "deal_int.h"
#include "extern.h"
#include "threads.h"
#include<queue>
#include<cstdlib>
#include<ctime>
using namespace std;
Deal_Int::Deal_Int()
{
    tid=pid=0;
    pos=-1;
}
Deal_Int::~Deal_Int()
{

}
double random2()
{
    return (double)rand()/RAND_MAX;
}
long long random2(long long m)
{
    return (long long)(random2()*(m)+0.5);
}
long long random2(long long x,long long y)
{
    long long tmp;
    while(1)
    {
        tmp=random2(y);
        if(tmp>=x) break;
    }
    return tmp;
}
void FIFO(int tid,int pos)
{
    TaskThread *p;
    vector<int> *page;
    if(tid==1) p=p1,page=&page_1;
    else if(tid==2) p=p2,page=&page_2;
    else if(tid==3) p=p3,page=&page_3;
    else p=p4,page=&page_4;
    queue<int> q;
    bool *vis=new bool[1048576+5];
    for(int i=0;i<1048576+5;i++) vis[i]=0;
    for(int i=0;i<=pos;i++)
    {
        if(int(q.size())<block_cnt)
        {
            vis[page->at(i)]=1;
            q.push(page->at(i));
        }
        else
        {
            if(vis[page->at(i)]) continue;
            else
            {
                int tmp=q.front();
                vis[tmp]=0;
                q.pop();
                q.push(page->at(i));
                vis[page->at(i)]=1;
                if(i==pos)
                {
                    for(int j=0;j<block_cnt;j++)
                    {
                        if(p->PT[j]==tmp)
                            p->PT[j]=page->at(pos);
                    }
                    for(int j=0;j<tlb_cnt;j++)
                    {
                        if(p->TLB[j]==tmp)
                            p->TLB[j]=0;
                    }
                }
            }
        }
    }
}
void LRU(int tid,int pos)
{
    TaskThread *p;
    vector<int> *page;
    if(tid==1) p=p1,page=&page_1;
    else if(tid==2) p=p2,page=&page_2;
    else if(tid==3) p=p3,page=&page_3;
    else p=p4,page=&page_4;
    int *tmp=new int[block_cnt+1];
    for(int i=0;i<block_cnt;i++)
        tmp[i]=-1;
    for(int i=pos-1;i>=0;i--)
    {
        for(int j=0;j<block_cnt;j++)
        {
            if(p->PT[j]==page->at(i)&&tmp[j]==-1)
            {
                tmp[j]=i;
            }
        }
    }

    int minpos=-1,minval=0x7f7f7f;
    for(int i=0;i<block_cnt;i++)
    {
        if(tmp[i]<minval)
        {
            minpos=i;
            minval=tmp[i];
        }
    }
    for(int j=0;j<tlb_cnt;j++)
    {
        if(p->TLB[j]==p->PT[minpos])
            p->TLB[j]=0;
    }
    p->PT[minpos]=page->at(pos);
    delete tmp;
}
void OPT(int tid,int pos)
{
    TaskThread *p;
    vector<int> *page;
    if(tid==1) p=p1,page=&page_1;
    else if(tid==2) p=p2,page=&page_2;
    else if(tid==3) p=p3,page=&page_3;
    else p=p4,page=&page_4;
    int *tmp=new int[block_cnt+1];
    for(int i=0;i<block_cnt;i++)
        tmp[i]=-1;
    for(unsigned int i=pos+1;i<page->size();i++)
    {
        for(int j=0;j<block_cnt;j++)
        {
            if(p->PT[j]==page->at(i)&&tmp[j]==-1)
            {
                tmp[j]=i;
            }
        }
    }
    for(int i=0;i<block_cnt;i++)
        if(tmp[i]==-1)
        {
            p->PT[i]=page->at(pos);
            return;
        }
    int maxpos=-1,maxval=-1;
    for(int i=0;i<block_cnt;i++)
    {
        if(tmp[i]>maxval)
        {
            maxpos=i;
            maxval=tmp[i];
        }
    }
    for(int j=0;j<tlb_cnt;j++)
    {
        if(p->TLB[j]==p->PT[maxpos])
            p->TLB[j]=0;
    }
    p->PT[maxpos]=page->at(pos);
    delete tmp;
}
void SLNN(int tid,int pos)  //不可行，用随机替代
{
    TaskThread *p;
    vector<int> *page;
    if(tid==1) p=p1,page=&page_1;
    else if(tid==2) p=p2,page=&page_2;
    else if(tid==3) p=p3,page=&page_3;
    else p=p4,page=&page_4;
    srand((unsigned)time(NULL));
    p->PT[int(random2(1,block_cnt))-1]=page->at(pos);
}
void Deal_Int::run()
{
    while(tid&&pid&&pos!=-1)
    {
        switch(tid)
        {
            case 1:
                if(!p1->PT_full)
                {
                    for(int j=0;j<block_cnt;j++)
                    {
                        if(p1->PT[j]==0)
                        {
                            p1->PT[j]=pid;
                            if(j==block_cnt-1) p1->PT_full=1;
                            break;
                        }
                    }
                }
                else
                {
                    switch(algorithm_1)
                    {
                        case 1:
                            FIFO(1,pos);
                            break;
                        case 2:
                            LRU(1,pos);
                            break;
                        case 3:
                            OPT(1,pos);
                            break;
                        default:
                            SLNN(1,pos);
                    }
                }
                p1->tot_time+=int_time;
            break;
            case 2:
                if(!p2->PT_full)
                {
                    for(int j=0;j<block_cnt;j++)
                    {
                        if(p2->PT[j]==0)
                        {
                            p2->PT[j]=pid;
                            if(j==block_cnt-1) p2->PT_full=1;
                            break;
                        }
                    }
                }
                else
                {
                    switch(algorithm_2)
                    {
                        case 1:
                            FIFO(2,pos);
                            break;
                        case 2:
                            LRU(2,pos);
                            break;
                        case 3:
                            OPT(2,pos);
                            break;
                        default:
                            SLNN(2,pos);
                    }
                }
                p2->tot_time+=int_time;
            break;
            case 3:
                if(!p3->PT_full)
                {
                    for(int j=0;j<block_cnt;j++)
                    {
                        if(p3->PT[j]==0)
                        {
                            p3->PT[j]=pid;
                            if(j==block_cnt-1) p3->PT_full=1;
                            break;
                        }
                    }
                }
                else
                {
                    switch(algorithm_3)
                    {
                        case 1:
                            FIFO(3,pos);
                            break;
                        case 2:
                            LRU(3,pos);
                            break;
                        case 3:
                            OPT(3,pos);
                            break;
                        default:
                            SLNN(3,pos);
                    }
                }
                p3->tot_time+=int_time;
            break;
            case 4:
                if(!p4->PT_full)
                {
                    for(int j=0;j<block_cnt;j++)
                    {
                        if(p4->PT[j]==0)
                        {
                            p4->PT[j]=pid;
                            if(j==block_cnt-1) p4->PT_full=1;
                            break;
                        }
                    }
                }
                else
                {
                    switch(algorithm_4)
                    {
                        case 1:
                            FIFO(4,pos);
                            break;
                        case 2:
                            LRU(4,pos);
                            break;
                        case 3:
                            OPT(4,pos);
                            break;
                        default:
                            SLNN(4,pos);
                    }
                }
                p4->tot_time+=int_time;
            break;
        }
        break;
    }
}
void Deal_Int::slotTid(int x)
{
     tid=x;
}
void Deal_Int::slotPid(int x)
{
     pid=x;
}
void Deal_Int::slotPos(int x)
{
     pos=x;
}

