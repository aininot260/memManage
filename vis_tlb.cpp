#include <QtDebug>
#include "vis_tlb.h"
#include "extern.h"
#include "threads.h"
Vis_TLB::Vis_TLB()
{
    tid=pid=0;
    connect(this, SIGNAL(signalsSetvt1(int)), p1, SLOT(slotSetvt(int)));
    connect(this, SIGNAL(signalsSetvt2(int)), p2, SLOT(slotSetvt(int)));
    connect(this, SIGNAL(signalsSetvt3(int)), p3, SLOT(slotSetvt(int)));
    connect(this, SIGNAL(signalsSetvt4(int)), p4, SLOT(slotSetvt(int)));
}
Vis_TLB::~Vis_TLB()
{

}
void Vis_TLB::run()
{
    while(tid&&pid)
    {
        int ff;
        switch(tid)
        {
            case 1:
                ff=0;
                for(int i=0;i<tlb_cnt;i++)
                {
                    if(pid==p1->TLB[i])
                    {
                        p1->flag=1;
                        ff=1;
                        emit signalsSetvt1(1);
                        //qDebug()<<1;
                        break;
                    }
                }
                if(ff==0) signalsSetvt1(0);
            break;
            case 2:
                ff=0;
                for(int i=0;i<tlb_cnt;i++)
                {
                    if(pid==p2->TLB[i])
                    {
                        p2->flag=1;
                        ff=1;
                        emit signalsSetvt2(1);
                        //qDebug()<<1;
                        break;
                    }
                }
                if(ff==0) signalsSetvt2(0);
            break;
            case 3:
                ff=0;
                for(int i=0;i<tlb_cnt;i++)
                {
                    if(pid==p3->TLB[i])
                    {
                        p3->flag=1;
                        ff=1;
                        emit signalsSetvt3(1);
                        //qDebug()<<1;
                        break;
                    }
                }
                if(ff==0) signalsSetvt3(0);
            break;
            case 4:
                ff=0;
                for(int i=0;i<tlb_cnt;i++)
                {
                    if(pid==p4->TLB[i])
                    {
                        p4->flag=1;
                        ff=1;
                        emit signalsSetvt4(1);
                        //qDebug()<<1;
                        break;
                    }
                }
                if(ff==0) signalsSetvt4(0);
            break;
        }
        break;
    }
}
void Vis_TLB::slotTid(int x)
{
     tid=x;
}
void Vis_TLB::slotPid(int x)
{
     pid=x;
}

