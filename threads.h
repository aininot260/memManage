#ifndef THREADS_H
#define THREADS_H
#include "task_thread.h"
#include "vis_mem.h"
#include "vis_pt.h"
#include "vis_tlb.h"
#include "deal_int.h"
extern TaskThread *p1,*p2,*p3,*p4;
extern Vis_TLB *vt1,*vt2,*vt3,*vt4;
extern Vis_PT *vp1,*vp2,*vp3,*vp4;
extern Deal_Int *di1,*di2,*di3,*di4;
extern Vis_Mem *vm1,*vm2,*vm3,*vm4;
#endif // THREADS_H
