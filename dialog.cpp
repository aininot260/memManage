#include "dialog.h"
#include "ui_dialog.h"
#include "extern.h"
#include<sstream>
#include<QMessageBox>
#include<map>
#include<cstdlib>
#include<ctime>
#include<QDebug>
#include<string>
using namespace std;

map<char,int> mp;
Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    for(int i=0;i<=9;i++)
        mp[char(i+'0')]=i;
    for(int i=65;i<=70;i++)
        mp[char(i)]=i-55;
    for(int i=97;i<=102;i++)
        mp[char(i)]=i-87;
}

Dialog::~Dialog()
{
    delete ui;
}

string del_H(string s)
{
    string ret;
    for(unsigned int i=0;i<s.size()-1;i++)
        ret=ret+s[i];
    return ret;
}
int get_power(int x,int n)
{
    int tmp=1;
    for(int i=1;i<=n;i++)
    {
        tmp*=x;
    }
    return tmp;
}
int base16_to_base10(string n)  //16进制转10进制
{
    int ans=0;
    int len=n.size();
    int m=16;
    for(int i=0;i<len;i++)
        ans+=mp[n[i]]*get_power(m,len-i-1);
    return ans;
}
double random()
{
    return (double)rand()/RAND_MAX;
}
long long random(long long m)
{
    return (long long)(random()*(m)+0.5);
}
long long random(long long x,long long y)
{
    long long tmp;
    while(1)
    {
        tmp=random(y);
        if(tmp>=x) break;
    }
    return tmp;
}
char* strrev(char* s)
{
    char* h=s;
    char* t=s;
    while(*t++);
    t-=2;
    while(h<t)
    {
        char ch=*h;
        *h++=*t;
        *t--=ch;
    }
    return s;
}
string base10_to_base16(int n)
{
    int m=16;
    char a[15]="",b[20];
    for(int i=0;i<=9;i++)
        b[i]=char(i+'0');
    for(int i=10;i<=15;i++)
        b[i]=char(i+55);
    int cnt=0;
    while(n)
    {
        a[cnt++]=b[n%m];
        n/=m;
    }
    strrev(a);
    string ret(a);
    ret=ret+'H';
    return ret;
}
void make_data(vector<string> &vec,int cnt,string l,string r)
{
    srand((unsigned)time(NULL));
    int ll=base16_to_base10(del_H(l));
    int rr=base16_to_base10(del_H(r));
    if(ll<=0||rr>1048576||rr-ll<5)
    {
        QMessageBox::warning(NULL, "警告", "输入数据边界有误，应保证页表编号[1,1048576]且应保证右值-左值≥5。", QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
        ll=1;
        rr=10;
    }
    int *vis=new int[1048576+5];
    for(int i=0;i<cnt;i++)
    {
        int tmp=random(ll,rr);
        vis[tmp]++;

        string s=base10_to_base16(tmp);
        vec.push_back(s);
    }
    for(int i=0;i<1048576;i++)
    {
        if(vis[i]==1)
        {
            string s=base10_to_base16(i);
            vec[random(0,cnt)]=s;
        }
    }
}

bool check(string s)
{
    if(s.size()<=1||s.size()>9)
    {
        QMessageBox::warning(NULL, "警告", "逻辑地址输入有误，请保证输入的数据在[1H,FFFFFFFFH]之间。", QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
        return false;
    }
    for(unsigned int i=0;i<s.size()-1;i++)
    {
        int flag=0;
        for(int j='0';j<='9';j++)
        {
            if(s[i]==j) flag++;
        }
        for(int j='A';j<='F';j++)
        {
            if(s[i]==j) flag++;
        }
        for(int j='a';j<='f';j++)
        {
            if(s[i]==j) flag++;
        }
        if(!flag)
        {
            QMessageBox::warning(NULL, "警告", "逻辑地址输入非法，请保证输入的数据是以H结尾的16进制数。", QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
            return false;
        }
    }
    int flag=0;
    if(s[s.size()-1]=='H'||s[s.size()-1]=='h') flag++;
    if(!flag)
    {
        QMessageBox::warning(NULL, "警告", "逻辑地址输入非法，请保证输入的数据是以H结尾的16进制数。", QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
        return false;
    }
    return true;
}
void Dialog::on_pushButton_clicked()
{
    is_cfg=1;
    mem_1.clear();
    mem_2.clear();
    mem_3.clear();
    mem_4.clear();
    page_1.clear();
    page_2.clear();
    page_3.clear();
    page_4.clear();

    block_cnt=ui->lineEditT1->text().toInt();
    if(block_cnt<2||block_cnt>10)
    {
        QMessageBox::warning(NULL, "警告", "驻留内存页面的个数输入有误，请保证输入的数据在[2,10]之间。", QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
        return;
    }
    int_time=ui->lineEditT2->text().toInt();
    if(int_time<0)
    {
        QMessageBox::warning(NULL, "警告", "缺页中断的时间输入有误，请保证输入的数据非负。", QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
        return;
    }
    mem_time=ui->lineEditT3->text().toInt();
    if(mem_time<0)
    {
        QMessageBox::warning(NULL, "警告", "内存的存取时间输入有误，请保证输入的数据非负。", QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
        return;
    }
    tlb_time=ui->lineEditT4->text().toInt();
    if(tlb_time<0)
    {
        QMessageBox::warning(NULL, "警告", "快表的访问时间输入有误，请保证输入的数据非负。", QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
        return;
    }
    tlb_cnt=ui->lineEditT5->text().toInt();
    if(tlb_cnt<0||tlb_cnt>block_cnt)
    {
        QMessageBox::warning(NULL, "警告", "快表的容量输入有误，请保证输入的数据非负且小于页表容量。", QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
        return;
    }


    if(ui->radioButton11->isChecked()) algorithm_1=1;
    if(ui->radioButton12->isChecked()) algorithm_1=2;
    if(ui->radioButton13->isChecked()) algorithm_1=3;
    if(ui->radioButton14->isChecked()) algorithm_1=4;
    if(ui->radioButton21->isChecked()) algorithm_2=1;
    if(ui->radioButton22->isChecked()) algorithm_2=2;
    if(ui->radioButton23->isChecked()) algorithm_2=3;
    if(ui->radioButton24->isChecked()) algorithm_2=4;
    if(ui->radioButton31->isChecked()) algorithm_3=1;
    if(ui->radioButton32->isChecked()) algorithm_3=2;
    if(ui->radioButton33->isChecked()) algorithm_3=3;
    if(ui->radioButton34->isChecked()) algorithm_3=4;
    if(ui->radioButton41->isChecked()) algorithm_4=1;
    if(ui->radioButton42->isChecked()) algorithm_4=2;
    if(ui->radioButton43->isChecked()) algorithm_4=3;
    if(ui->radioButton44->isChecked()) algorithm_4=4;

    if(ui->checkBox11->isChecked()) is_tlb_1=1;
        else is_tlb_1=0;
    if(ui->checkBox21->isChecked()) is_tlb_2=1;
        else is_tlb_2=0;
    if(ui->checkBox31->isChecked()) is_tlb_3=1;
        else is_tlb_3=0;
    if(ui->checkBox41->isChecked()) is_tlb_4=1;
        else is_tlb_4=0;

    int tmp1;
    string tmp,tmp2,tmp3;
    if(!ui->checkBox12->isChecked())
    {
        stringstream ss1(ui->textEdit1->toPlainText().toStdString());
        while(ss1>>tmp)
        {
            mem_1.push_back(tmp);
            if(!check(tmp))
            {
                mem_1.clear();
                return;
            }
        }
    }
    else
    {
        tmp1=ui->lineEdit11->text().toInt();
        tmp2=ui->lineEdit12->text().toStdString();
        tmp3=ui->lineEdit13->text().toStdString();
        if(tmp1<=0||tmp1>1000)
        {
            QMessageBox::warning(NULL, "警告", "随机逻辑地址数量输入有误，请保证输入的数据范围[1,1000]。", QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
            return;
        }
        if(check(tmp2)&&check(tmp3))
        {
            make_data(mem_1,tmp1,tmp2,tmp3);
        }
        else return;
    }
    if(!ui->checkBox22->isChecked())
    {
        stringstream ss2(ui->textEdit2->toPlainText().toStdString());
        while(ss2>>tmp)
        {
            mem_2.push_back(tmp);
            if(!check(tmp))
            {
                mem_2.clear();
                return;
            }
        }
    }
    else
    {   tmp1=ui->lineEdit21->text().toInt();
        tmp2=ui->lineEdit22->text().toStdString();
        tmp3=ui->lineEdit23->text().toStdString();
        if(tmp1<=0||tmp1>=1000)
        {
            QMessageBox::warning(NULL, "警告", "随机逻辑地址数量输入有误，请保证输入的数据范围[1,1000]。", QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
            return;
        }
        if(check(tmp2)&&check(tmp3))
            make_data(mem_2,tmp1,tmp2,tmp3);
        else return;
    }
    if(!ui->checkBox32->isChecked())
    {
            stringstream ss3(ui->textEdit3->toPlainText().toStdString());
            while(ss3>>tmp)
            {
                mem_3.push_back(tmp);
                if(!check(tmp))
                {
                    mem_3.clear();
                    return;
                }
            }
    }
    else
    {
        tmp1=ui->lineEdit31->text().toInt();
        tmp2=ui->lineEdit32->text().toStdString();
        tmp3=ui->lineEdit33->text().toStdString();
        if(tmp1<=0||tmp1>=1000)
        {
            QMessageBox::warning(NULL, "警告", "随机逻辑地址数量输入有误，请保证输入的数据范围[1,1000]。", QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
            return;
        }
        if(check(tmp2)&&check(tmp3))
            make_data(mem_3,tmp1,tmp2,tmp3);
        else return;
    }
    if(!ui->checkBox42->isChecked())
    {
        stringstream ss4(ui->textEdit4->toPlainText().toStdString());
        while(ss4>>tmp)
        {
            mem_4.push_back(tmp);
            if(!check(tmp))
            {
                mem_4.clear();
                return;
            }
        }
    }
    else
    {
        tmp1=ui->lineEdit41->text().toInt();
        tmp2=ui->lineEdit42->text().toStdString();
        tmp3=ui->lineEdit43->text().toStdString();
        if(tmp1<=0||tmp1>=1000)
        {
            QMessageBox::warning(NULL, "警告", "随机逻辑地址数量输入有误，请保证输入的数据范围[1,1000]。", QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
            return;
        }
        if(check(tmp2)&&check(tmp3))
            make_data(mem_4,tmp1,tmp2,tmp3);
        else return;
    }
    if(mem_1.size()==0||mem_2.size()==0||mem_3.size()==0||mem_4.size()==0)
    {
        QMessageBox::warning(NULL, "警告", "请保证输入的逻辑地址是正确的。", QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
        return;
    }
    for(unsigned int i=0;i<mem_1.size();i++)
    {
        tmp=del_H(mem_1[i]);
        tmp1=base16_to_base10(tmp);
        tmp1>>=12;
        page_1.push_back(tmp1);
    }
    for(unsigned int i=0;i<mem_2.size();i++)
    {
        tmp=del_H(mem_2[i]);
        tmp1=base16_to_base10(tmp);
        tmp1>>=12;
        page_2.push_back(tmp1);
    }
    for(unsigned int i=0;i<mem_3.size();i++)
    {
        tmp=del_H(mem_3[i]);
        tmp1=base16_to_base10(tmp);
        tmp1>>=12;
        page_3.push_back(tmp1);
    }
    for(unsigned int i=0;i<mem_4.size();i++)
    {
        tmp=del_H(mem_4[i]);
        tmp1=base16_to_base10(tmp);
        tmp1>>=12;
        page_4.push_back(tmp1);
    }
    this->close();
}

void Dialog::on_checkBox12_clicked(bool checked)
{
    if(checked)
    {
        ui->lineEdit11->setEnabled(true);
        ui->lineEdit12->setEnabled(true);
        ui->lineEdit13->setEnabled(true);
        ui->textEdit1->setEnabled(false);
    }
    else
    {
        ui->lineEdit11->setEnabled(false);
        ui->lineEdit12->setEnabled(false);
        ui->lineEdit13->setEnabled(false);
        ui->textEdit1->setEnabled(true);
    }
}

void Dialog::on_checkBox22_clicked(bool checked)
{
    if(checked)
    {
        ui->lineEdit21->setEnabled(true);
        ui->lineEdit22->setEnabled(true);
        ui->lineEdit23->setEnabled(true);
        ui->textEdit2->setEnabled(false);
    }
    else
    {
        ui->lineEdit21->setEnabled(false);
        ui->lineEdit22->setEnabled(false);
        ui->lineEdit23->setEnabled(false);
        ui->textEdit2->setEnabled(true);
    }
}

void Dialog::on_checkBox32_clicked(bool checked)
{
    if(checked)
    {
        ui->lineEdit31->setEnabled(true);
        ui->lineEdit32->setEnabled(true);
        ui->lineEdit33->setEnabled(true);
        ui->textEdit3->setEnabled(false);
    }
    else
    {
        ui->lineEdit31->setEnabled(false);
        ui->lineEdit32->setEnabled(false);
        ui->lineEdit33->setEnabled(false);
        ui->textEdit3->setEnabled(true);
    }
}

void Dialog::on_checkBox42_clicked(bool checked)
{
    if(checked)
    {
        ui->lineEdit41->setEnabled(true);
        ui->lineEdit42->setEnabled(true);
        ui->lineEdit43->setEnabled(true);
        ui->textEdit4->setEnabled(false);
    }
    else
    {
        ui->lineEdit41->setEnabled(false);
        ui->lineEdit42->setEnabled(false);
        ui->lineEdit43->setEnabled(false);
        ui->textEdit4->setEnabled(true);
    }
}
