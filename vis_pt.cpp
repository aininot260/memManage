#include <QtDebug>
#include "extern.h"
#include "vis_pt.h"
#include "threads.h"
#include<ctime>
#include<cstdlib>
Vis_PT::Vis_PT()
{
    tid=pid=0;
    connect(this, SIGNAL(signalsSetvp1(int)), p1, SLOT(slotSetvp(int)));
    connect(this, SIGNAL(signalsSetvp2(int)), p2, SLOT(slotSetvp(int)));
    connect(this, SIGNAL(signalsSetvp3(int)), p3, SLOT(slotSetvp(int)));
    connect(this, SIGNAL(signalsSetvp4(int)), p4, SLOT(slotSetvp(int)));
}
Vis_PT::~Vis_PT()
{

}
double random1()
{
    return (double)rand()/RAND_MAX;
}
int random1(int m)
{
    return (int)(random1()*(m)+0.5);
}
void Vis_PT::run()
{
    while(tid&&pid)
    {
        int ff;
        switch(tid)
        {
            case 1:
                ff=0;
                for(int i=0;i<block_cnt;i++)
                {
                    if(pid==p1->PT[i])
                    {
                        sleep(1);
                        //qDebug()<<2;
                        ff=1;
                        emit signalsSetvp1(1);
                        //修改快表
                        if(!p1->TLB_full)
                        {
                            for(int j=0;j<tlb_cnt;j++)
                            {
                                if(p1->TLB[j]==0)
                                {
                                    p1->TLB[j]=pid;
                                    if(j==tlb_cnt-1) p1->TLB_full=1;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            srand((unsigned)time(NULL));
                            p1->TLB[random1(tlb_cnt)-1]=pid;
                        }
                        p1->flag=1;
                        break;
                    }
                }
                if(ff==0) emit signalsSetvp1(0);
            break;
            case 2:
                ff=0;
                for(int i=0;i<block_cnt;i++)
                {
                    if(pid==p2->PT[i])
                    {
                        sleep(1);
                        //qDebug()<<2;
                        ff=1;
                        emit signalsSetvp2(1);
                        //修改快表
                        if(!p2->TLB_full)
                        {
                            for(int j=0;j<tlb_cnt;j++)
                            {
                                if(p2->TLB[j]==0)
                                {
                                    p2->TLB[j]=pid;
                                    if(j==tlb_cnt-1) p2->TLB_full=1;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            srand((unsigned)time(NULL));
                            p2->TLB[random1(tlb_cnt)-1]=pid;
                        }
                        p2->flag=1;
                        break;
                    }
                }
                if(ff==0) emit signalsSetvp2(0);
            break;
            case 3:
                ff=0;
                for(int i=0;i<block_cnt;i++)
                {
                    if(pid==p3->PT[i])
                    {
                        sleep(1);
                        //qDebug()<<2;
                        ff=1;
                        emit signalsSetvp3(1);
                        //修改快表
                        if(!p3->TLB_full)
                        {
                            for(int j=0;j<tlb_cnt;j++)
                            {
                                if(p3->TLB[j]==0)
                                {
                                    p3->TLB[j]=pid;
                                    if(j==tlb_cnt-1) p3->TLB_full=1;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            srand((unsigned)time(NULL));
                            p3->TLB[random1(tlb_cnt)-1]=pid;
                        }
                        p3->flag=1;
                        break;
                    }
                }
                if(ff==0) emit signalsSetvp3(0);
            break;
            case 4:
                ff=0;
                for(int i=0;i<block_cnt;i++)
                {
                    if(pid==p4->PT[i])
                    {
                        sleep(1);
                        //qDebug()<<2;
                        ff=1;
                        emit signalsSetvp4(1);
                        //修改快表
                        if(!p4->TLB_full)
                        {
                            for(int j=0;j<tlb_cnt;j++)
                            {
                                if(p4->TLB[j]==0)
                                {
                                    p4->TLB[j]=pid;
                                    if(j==tlb_cnt-1) p4->TLB_full=1;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            srand((unsigned)time(NULL));
                            p4->TLB[random1(tlb_cnt)-1]=pid;
                        }
                        p4->flag=1;
                        break;
                    }
                }
                if(ff==0) emit signalsSetvp4(0);
            break;
        }
        break;
    }
}
void Vis_PT::slotTid(int x)
{
     tid=x;
}
void Vis_PT::slotPid(int x)
{
     pid=x;
}
