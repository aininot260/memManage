#ifndef VIS_PT_H
#define VIS_PT_H
#include <QThread>
#include "widget.h"
class Vis_PT : public QThread
{
    Q_OBJECT
public:
    Vis_PT();
    ~Vis_PT();
    int tid,pid;
protected:
    void run();
signals:
    signalsSetvp1(int);
    signalsSetvp2(int);
    signalsSetvp3(int);
    signalsSetvp4(int);
public slots:
    void slotTid(int);
    void slotPid(int);
};
#endif // VIS_PT_H
